#!/bin/bash

rootDir=`pwd`

mkdir -p $rootDir/build/someLib     || exit 1
mkdir -p $rootDir/build/splitProj   || exit 1
mkdir -p $rootDir/install           || exit 1

cd $rootDir/build/someLib || exit 1
cmake -DCMAKE_INSTALL_PREFIX=$rootDir/install \
        $rootDir/someLib || exit 2
make || exit 3
make install || exit 4

cd $rootDir/build/splitProj || exit 1
cmake -DCMAKE_INSTALL_PREFIX=$rootDir/install \
        -DCMAKE_PREFIX_PATH=$rootDir/build/someLib \
        $rootDir/splitProj || exit 2
make || exit 3
make install || exit 4

cd $rootDir || exit 1
