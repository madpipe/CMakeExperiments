find_package(someLib CONFIG REQUIRED)

set(splitProjLib_SRCS
    ${CMAKE_CURRENT_SOURCE_DIR}/Sources/smart.cpp )

set(splitProj_SRCS
    ${CMAKE_CURRENT_SOURCE_DIR}/Sources/main.cpp)

add_library(splitProjLib ${splitProjLib_SRCS})
target_include_directories(splitProjLib PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/Includes")
target_link_libraries(splitProjLib
        someLib::someLib)
set_property(TARGET splitProjLib PROPERTY ENABLE_EXPORTS ON)

add_executable(splitProj ${splitProj_SRCS})
target_link_libraries(splitProj splitProjLib)

install(TARGETS splitProj DESTINATION bin)
